package com.company;

import java.util.Collections;

import static com.company.PlaneReservation.PlaceType.STANDARD;
import static com.company.PlaneReservation.PlaceType.VIP;

public class Main {

    public static void main(String[] args) {
        Plane plane = new Plane();

        plane.reservation(VIP,"test",1200);
        plane.reservation(VIP,"ziomek",1200);

        plane.reservation(VIP,"trzeci",1200);
        plane.reservation(VIP,"czwarty",1200);
        plane.reservation(VIP,"piaty",1200);
        plane.reservation(VIP,"szosty",1200);
        plane.reservation(VIP,"siodmy",1200);

        plane.cancel(1);
        plane.reservation(VIP,"usuniety",1200);

        plane.reservation(STANDARD,"bidula",800);
        plane.reservation(STANDARD,"no",800);
        plane.reservation(STANDARD,"no",800);
        plane.reservation(STANDARD,"no",800);
        plane.reservation(STANDARD,"no",800);
        plane.reservation(STANDARD,"no",800);
        plane.reservation(STANDARD,"no",800);


        for(int i=0;i<plane.vip_places.size();i++){
            System.out.println(plane.vip_places.get(i).toString() + " index: "+i);
        }
        for(int i=0;i<plane.standard_places.size();i++){
            System.out.println(plane.standard_places.get(i).toString());
        }

        System.out.println(plane.status());
        System.out.println(plane.freePlaces());
        System.out.println(plane.reservedPlaces());

        System.out.println(" ");
        plane.changePlaces(3,5);
        for(int i=0;i<plane.vip_places.size();i++){
            System.out.println(plane.vip_places.get(i).toString() + " index: "+i);
        }
    }
}
