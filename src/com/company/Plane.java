package com.company;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.company.PlaneReservation.PlaceType.STANDARD;
import static com.company.PlaneReservation.PlaceType.VIP;

public class Plane implements PlaneReservation {
    PlaceType placeType;
    String username;
    int price;

    int vip_size=28;
    int standard_size=100;
    int vip_id=1;
    int standard_id=vip_size+1;

    List<Place> vip_places= new ArrayList<>();
    List<Place> standard_places= new ArrayList<>();

    List<Place> vip_free_places= new ArrayList<>();
    List<Place> standard_free_places= new ArrayList<>();
    Plane(){
        for (int i=1;i<=vip_size;i++){
            Place templacer=new Place(i,null,VIP);
            vip_free_places.add(templacer);
        }
        for (int i=vip_size+1;i<=standard_size+vip_size;i++){
            Place templacer=new Place(i,null,STANDARD);
            standard_free_places.add(templacer);
        }
    }

    int status=0;

    @Override
    public Optional<Integer> reservation(PlaceType placeType, String username, int price) {
        this.placeType=placeType;
        this.username=username;
        this.price=price;

        if(placeType== VIP ){
            if(vip_places.size()<vip_size){
                if(vip_free_places.isEmpty())  vip_places.add(new Place(vip_id++,username,placeType));
                else{
                    Place templace=vip_free_places.get(0);
                    vip_places.add(new Place(templace.id,username,placeType));
                    vip_free_places.remove(0);
                }
                status+=price;
            }
        }
        else if(placeType==STANDARD ){
            if(standard_places.size()<standard_size){
                if(standard_free_places.isEmpty()) standard_places.add(new Place(standard_id++,username,placeType));
                else{
                    Place templace=standard_free_places.get(0);
                    standard_places.add(new Place(templace.id,username,placeType));
                    standard_free_places.remove(0);
                }
                status+=price;
            }
        }
        else return Optional.empty();
        return Optional.empty();
    }

    @Override
    public Optional<Integer> cancel(int placeId) {
        Place templaces= vip_places.get(placeId-1);
        vip_free_places.add(templaces);
        vip_places.remove(placeId-1);
        if(templaces.placeType==VIP) status-=1200;
        else status-=800;
        return Optional.empty();
    }

    @Override
    public int status() {
        return status*100;
    }

    @Override
    public List<Place> freePlaces() {
        return Stream.concat(vip_free_places.stream(), standard_free_places.stream())
                .collect(Collectors.toList());
    }

    @Override
    public List<Place> reservedPlaces() {
        return Stream.concat(vip_places.stream(), standard_places.stream())
                .collect(Collectors.toList());
    }

    @Override
    public boolean changePlaces(int sourcePlaceId, int destinationPlaceId) {
        Integer source=null,destination=null;
        for (int i=0;i<vip_places.size();i++){
            Place kupa= vip_places.get(i);
            if(kupa.id==sourcePlaceId){
                source=vip_places.indexOf(kupa);
            }
            if(kupa.id==destinationPlaceId){
                destination=vip_places.indexOf(kupa);
            }
        }
        if(source!=null && destination!=null) {
            Place source_templace = vip_places.get(source);
            Place destination_templace = vip_places.get(destination);
            Place source_changer = new Place(destinationPlaceId, source_templace.username, source_templace.placeType);
            Place destination_changer = new Place(sourcePlaceId, destination_templace.username, destination_templace.placeType);
            vip_places.set(source, source_changer);
            vip_places.set(destination, destination_changer);
            return true;
        }
        else return false;
    }
}

