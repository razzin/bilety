package com.company;

import java.util.List;
import java.util.Optional;

class Place {
    int id;
    String username;
    PlaneReservation.PlaceType placeType;

    public Place(int id, String username, PlaneReservation.PlaceType placeType) {
        this.id = id;
        this.username = username;
        this.placeType = placeType;
    }
    @Override
    public String toString(){    //testowe wypisywanie miejsc
        return this.id+ " " + this.username + " " + this.placeType;
    }
}

public interface PlaneReservation {

    enum PlaceType {
        STANDARD,
        VIP
    }

    Optional<Integer> reservation(final PlaceType placeType, final String username, int price);
    Optional<Integer> cancel(int placeId);
    int status();
    List<Place> freePlaces();
    List<Place> reservedPlaces();
    boolean changePlaces(int sourcePlaceId, int destinationPlaceId);
}
